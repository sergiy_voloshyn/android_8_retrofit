package com.sourceit.retrofitexample;

/**
 * Created by Student on 27.01.2018.
 */

public class Country {
   public String name;
   public String capital;
   public String region;

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
