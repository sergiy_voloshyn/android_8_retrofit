package com.sourceit.retrofitexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Retrofit.getCountries(new Callback<List<Country>>() {
                                  @Override
                                  public void success(List<Country> countries, Response response) {

                                      Toast.makeText(MainActivity.this,countries.get(0).toString(),Toast.LENGTH_SHORT).show();


                                  }

                                  @Override
                                  public void failure(RetrofitError error) {
                                      Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                                  }
                              }
            );

        Retrofit.getCountriesByName(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {
                Toast.makeText(MainActivity.this,countries.get(0).toString(),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });




    }
}
